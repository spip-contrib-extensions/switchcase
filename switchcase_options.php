<?php

/**
 * Balises SPIP génériques supplémentaires
 *
 * @copyright  2015-2023
 * @author     JLuc chez no-log.org
 * @licence    GPL
 */

function balise_SWITCH_dist($p) {
	$_val = interprete_argument_balise(1, $p);
	if ($_val === NULL) {
		$err = array('zbug_balise_sans_argument', array('balise' => ' #SWITCH'));
		erreur_squelette($err, $p);
	}
	else {
		$p->code = "(vide(\$Pile['vars']['_switch_'] = $_val).vide(\$Pile['vars']['_switch_matched_']=''))";
		// #GET{_switch_} renvoie maintenant la valeur testée
		// et #GET{_switch_matched_} indique si un test #CASE a déjà été satisfait
	}

	$p->interdire_script = false;
	return $p;
}

function balise_CASE_dist($p) {
	$cases = [];
	$i=1;
	while ($tested = interprete_argument_balise($i++, $p)) {
		$cases[] = $tested;
	}
	if (!$cases) {
		$err = array('zbug_balise_sans_argument', array('balise' => ' #CASES'));
		erreur_squelette($err, $p);
	}
	else {
		$cases = 'array( '.implode (',', $cases).' )';
		$p->code = "(in_array(\$Pile['vars']['_switch_'], $cases) ? ' '.vide(\$Pile['vars']['_switch_matched_']=' ') : '')";
	}
	$p->interdire_script = false;
	return $p;
}

function balise_CASE_DEFAULT_dist($p) {
	$p->code = "(\$Pile['vars']['_switch_matched_'] ? '' : ' ')";
	$p->interdire_script = false;
	return $p;
}

